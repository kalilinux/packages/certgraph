module github.com/lanrat/certgraph

require (
	github.com/lib/pq v1.0.0
	github.com/weppos/publicsuffix-go v0.4.0
	golang.org/x/net v0.0.0-20180906233101-161cd47e91fd // indirect
	golang.org/x/text v0.3.0 // indirect
)
